﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.DAL
{
    public class OrderRepository : BaseRepository<Order>
    {
        //USE DI
        public OrderRepository(ILogger logger,string connectionString) : base(logger, connectionString)
        {}

        public Order GetOrderIformation(string orderCode)
        {
            using (IDbConnection connection = GetOpendConnection())
            {
                string query = $"SELECT TOP(1) OrderID, CustomerID, TotalMoney FROM dbo.Orders where OrderCode='{orderCode}'";
                var dataReader = base.Execute(connection, query);
                if (dataReader.Read())
                {
                    try
                    {
                        int index = 0;
                        var order = new Order(dataReader.GetString(index++), dataReader.GetString(index++), dataReader.GetDecimal(index++));
                        return order;

                    }
                    catch (Exception ex)
                    {
                        Logger.Log($"Cannot map sql values to type Order with orderCode {orderCode}", ex);
                        throw ex;
                    }
                }

                return null;
            }
        }
    }
}
