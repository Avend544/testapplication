﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;


namespace Example.DAL
{
    public abstract class BaseRepository<T>
    {
        protected ILogger Logger;
        public readonly string ConnectionString;
        public BaseRepository(ILogger logger, string connectionString)
        {
            Logger = logger;
            ConnectionString = connectionString;
        }

        protected IDbConnection GetOpendConnection()
        {
            try
            {
                IDbConnection connection = new SqlConnection(ConnectionString);
                connection.Open();
                return connection;
            }
            catch (InvalidOperationException ioex)
            {
                Logger.Log($"Cannot opend Sql connection. Connection string {ConnectionString}", ioex);
                throw ioex;
            }
            catch (SqlException sqlException)
            {
                Logger.Log($"A connection-level error occurred while opening the connection {ConnectionString}", sqlException);
                throw sqlException;
            }
        }

        public IDataReader Execute(IDbConnection connection, string query)
        {
            SqlCommand command = new SqlCommand(query, (SqlConnection)connection);
            connection.Open();
            return command.ExecuteReader();
        }
    }
}
