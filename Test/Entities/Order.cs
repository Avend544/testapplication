﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example
{
    public class Order
    {
        public Order()
        {

        }
        public Order(string orderID, string customerID, decimal totalMoney)
        {
            OrderID = orderID;
            CustomerID = customerID;
            TotalMoney = totalMoney;
        }

        public string OrderID { get; set; }
        public string CustomerID { get; set; }
        public decimal TotalMoney { get; set; }
    }
}
