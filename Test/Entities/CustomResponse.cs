﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Entities
{
    public class CustomResponse<TResponse>
    {
        public ResponseStatus Status { get; set; }
        public TResponse Response { get; set; }
        public string ErrorMessage { get; set; }
    }
}
