﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Entities
{
    public enum  ResponseStatus
    {
        Success = 1,
        Error = 2,
        NotFound = 3,
    }
}
