﻿using System.Diagnostics;

namespace Example
{
    public class StopwatchWrapper
    {
        private ILogger _logger;
        private Stopwatch _stopwatch;

        public StopwatchWrapper(ILogger logger)
        {
            _logger = logger;
            _stopwatch = new Stopwatch();
        }

        public void Start()
        {
            _stopwatch.Start();
        }

        public void StopWithLog()
        {
            _stopwatch.Stop();
            _logger.Log($"Elapsed - {_stopwatch.Elapsed}");
        }
    }
}
