﻿using Example.Entities;

namespace Example.Common
{
    public class ResponseMaker
    {
        public static CustomResponse<TResponse> GetSuccessResponse<TResponse>(TResponse response) where TResponse : new()
        {
            return new CustomResponse<TResponse>()
            {
                Status = ResponseStatus.Success,
                Response = response
            };
        }

        public static CustomResponse<TResponse> GetErrorResponse<TResponse>(string errorMessage)
        {
            return new CustomResponse<TResponse>()
            {
                Status = ResponseStatus.Error,
                ErrorMessage = errorMessage,
                Response = default(TResponse)
            };
        }

        public static CustomResponse<TResponse> GetNotFoundResponse<TResponse>(string message) where TResponse : new()
        {
            return new CustomResponse<TResponse>()
            {
                Status = ResponseStatus.NotFound,
                ErrorMessage = message,
                Response = default(TResponse)
            };
        }
    }
}
