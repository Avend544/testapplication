﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Web.Services;
using Example.BLL;
using Example.BLL.Contracts;
using Example.Common;
using Example.Entities;

namespace Example
{
    public class TempController
    {
        private readonly ILogger _logger;
        private readonly IOrderLogic _orderLogic;
        //USE DI
        public TempController(ILogger logger, StopwatchWrapper stopwatchWrapper, IOrderLogic orderLogic)
        {
            _logger = logger;
            _orderLogic = orderLogic;
        }

        [WebMethod]
        public CustomResponse<Order> LoadOrderInfo(string orderCode)
        {
            if (string.IsNullOrEmpty(orderCode))
            {
                return ResponseMaker.GetErrorResponse<Order>("Order code cannot be null or empty.");
            }

            StopwatchWrapper stopWatch = new StopwatchWrapper(_logger);

            try
            {
                stopWatch.Start();

                var responseData = _orderLogic.GetOrderFromCache(orderCode);
                if (responseData is null)
                {
                    return ResponseMaker.GetNotFoundResponse<Order>($"Order with code {orderCode} not found");
                }

                return ResponseMaker.GetSuccessResponse(responseData);
            }
            catch (Exception ex)
            {
                var errorMessage = $"Failed to get inforamtion of order with code { orderCode}";
                _logger.Log(errorMessage, ex);

                return ResponseMaker.GetErrorResponse<Order>(errorMessage);
            }
            finally
            {
                stopWatch.StopWithLog();
            }
        }
    }
}
