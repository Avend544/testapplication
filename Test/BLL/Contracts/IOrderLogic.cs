﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.BLL.Contracts
{
    public interface IOrderLogic
    {
        bool TryAddToCache(string key, Order order);

        bool TryRemoveToCache(string key, Order order);
        Order GetOrderFromCache(string orderCode);
    }
}
