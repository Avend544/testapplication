﻿using System.Collections.Concurrent;
using Example.BLL.Contracts;
using Example.DAL;

namespace Example.BLL
{
    public class OrderLogic : IOrderLogic
    {
        private static ConcurrentDictionary<string, Order> _cache =  new ConcurrentDictionary<string, Order>();
        private readonly OrderRepository _orderRepository;

        //USE DI
        public OrderLogic(OrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public bool TryAddToCache(string key, Order order)
        {
            return _cache.TryAdd(key, order);
        }

        public bool TryRemoveToCache(string key, Order order)
        {
            return _cache.TryRemove(key, out order);
        }

        public Order GetOrderFromCache(string orderCode)
        {
            if (_cache.ContainsKey(orderCode))
            {
                return _cache[orderCode];
            }

            Order order = _orderRepository.GetOrderIformation(orderCode);

            if (order is null)
            {
                return null;
            }

            if (!_cache.ContainsKey(orderCode))
            {
                _cache.TryAdd(orderCode, order);
            }

            return order;
        }
    }
}
