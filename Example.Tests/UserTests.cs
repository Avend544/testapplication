﻿using NUnit.Framework;
using Moq;
using System;
using System.Threading.Tasks;
using Example.Tests;

namespace NUnitTestProject1
{
    [TestFixture]
    public class UserTests
    {
        private Mock<IAuthenticationService> _mockAuthenticationService;
        private User _user;
        private int _secToExpire = 10;

        [SetUp]
        public void Setup()
        {
            string username = "admin";
            string password = "password";
            _mockAuthenticationService = new Mock<IAuthenticationService>();
            _mockAuthenticationService.SetupSequence(p => p.Authenticate(username, password)).Returns(
                new AuthToken()
                {
                    Token = Guid.NewGuid().ToString(),
                    ExpiresAt = DateTime.UtcNow.AddSeconds(_secToExpire)
                }).Returns(
                new AuthToken()
                {
                    Token = Guid.NewGuid().ToString(),
                    ExpiresAt = DateTime.UtcNow.AddMinutes(1)
                });

            _user = new User(_mockAuthenticationService.Object, username, password);
        }

        [Test]
        public async Task GetTokenTest()
        {
            string token1 = _user.AuthToken;

            await Task.Delay(_secToExpire * 1000 + 1000);

            string token2 = _user.AuthToken;

            await Task.Delay(_secToExpire * 1000);

            string token3 = _user.AuthToken;

            Assert.AreNotEqual(token1, token2);
            Assert.AreEqual(token2, token3);
        }
    }
}

