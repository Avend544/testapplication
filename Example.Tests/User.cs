﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Tests
{

    public class AuthToken
    {
        /// <summary>
        /// Authentication token string.
        /// </summary>
        public String Token { get; set; }
        /// <summary>
        /// Date and time the token expires at (UTC).
        /// </summary>
        public DateTime ExpiresAt { get; set; }
    }

    public interface IAuthenticationService
    {
        /// <summary>
        /// Authenticates user.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        /// <returns>Authentication token.</returns>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown when <paramref name="username"/> or <paramref name="password"/> is null.
        /// </exception>
        AuthToken Authenticate(string username, string password);
    }

    public interface IUser
    {
        /// <summary>
        /// User's authentication token.
        /// </summary>
        string AuthToken { get; }
    }

    public class User : IUser
    {
        private AuthToken _authToken;
        private readonly IAuthenticationService _authenticationService;
        public readonly string Username;
        public readonly string Password;

        public User(IAuthenticationService authenticationService, string username, string password)
        {
            _authenticationService = authenticationService;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                throw new ArgumentNullException("Username or password cannot be null.");

            Username = username;
            Password = password;
        }

        public string AuthToken
        {
            get
            {
                if (_authToken is null || _authToken.ExpiresAt < DateTime.UtcNow)
                    _authToken = _authenticationService.Authenticate(Username, Password);

                return _authToken.Token;
            }
        }
    }
}


